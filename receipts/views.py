from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import (
    CreateReceiptForm,
    CreateExpenseCategoryForm,
    CreateAccountForm,
)
from django.contrib.auth.decorators import login_required


@login_required
def receipt_listview(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipts}
    return render(request, "receipts/receiptslist.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")

    else:
        form = CreateReceiptForm()

    context = {"createreceipt": form}

    return render(request, "receipts/createreceipt.html", context)


def category_view(request):
    list = ExpenseCategory.objects.filter(owner=request.user)

    context = {"categories": list}

    return render(request, "receipts/categorylist.html", context)


def account_list(request):
    list = Account.objects.filter(owner=request.user)

    context = {"account": list}
    return render(request, "receipts/accountlist.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CreateExpenseCategoryForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.owner = request.user
            receipt.save()
            return redirect("category_list")
    else:
        form = CreateExpenseCategoryForm()

    context = {"createcategory": form}

    return render(request, "receipts/createcategory.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.owner = request.user
            receipt.save()
            return redirect("account_list")

    else:
        form = CreateAccountForm()

    context = {"createaccount": form}

    return render(request, "receipts/createaccount.html", context)
