# Generated by Django 4.1.5 on 2023-01-19 21:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0001_initial"),
    ]

    operations = [
        migrations.RenameField(
            model_name="receipt",
            old_name="purchase",
            new_name="purchaser",
        ),
    ]
